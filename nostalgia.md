---
layout: emo
title: "The fox hole"
---

# ![skull]({{'/assets/img/skullspin.gif' | absolute_url}} "skull") Welcome to the Lair ![skull]({{'/assets/img/skullspin.gif' | absolute_url}} "skull") 
---

**Just kidding, this is just a collection of 2000's nostalgia and what not that I just felt like putting here. I hope you like it too.** 
{: .text-color} 

**^-^**
{: .text-color} 

![heartagram]({{'/assets/img/heartagram.gif' | absolute_url}} "heartagram") 

![heartagram]({{'/assets/img/himlogo.jpg' | absolute_url}} "heartagram")  

![heartagram]({{'/assets/img/himalbum.jpg' | absolute_url}} "heartagram") 

![fall out boy FRom Under the Cork Tree]({{'/assets/img/falloutboy.jpg' | absolute_url}} "fall out boy From Under the Cork tree") 

![happy bunny]({{'/assets/img/happybunny.jpg' | absolute_url}} "happy bunny") 

![Linkin Park Hybrid Theory]({{'/assets/img/linkinpark.jpeg' | absolute_url}} "Linkin Park Hybrid Theory") 

![Evanesence Fallen]({{'/assets/img/Fallen.jpg' | absolute_url}} "Evanesence Fallen") 

![Evanesence]({{'/assets/img/evanesence.gif' | absolute_url}} "Evanesence") 

---

**By the way, I was much more open about being a weeb as a kid, so I couldn't resist adding some anime/manga gifs and pictures in here.**
{: .text-color} 

![Loveless]({{'/assets/img/loveless.gif' | absolute_url}} "Loveless") 

![L from Death Note]({{'/assets/img/L.gif' | absolute_url}} "L from Death Note") 

![Yaoi]({{'/assets/img/yaoi.gif' | absolute_url}} "Yaoi") 

![Chobits]({{'/assets/img/Chobits.jpg' | absolute_url}} "Chobits") 

![Hellsing]({{'/assets/img/Hellsing.jpg' | absolute_url}} "Hellsing") 

![Inuyasha]({{'/assets/img/Inuyasha.jpeg' | absolute_url}} "Inuyasha") 

**Before I continue sharing images and gifs from anime and manga I enjoyed in the 2000's I want to give you some more context for the next manga I have images from, Bizenghast. I really enjoyed the dark themes in both the story and the artwork. If you wanna learn more about it here's a [link](https://en.wikipedia.org/wiki/Bizenghast) to the wikipedia page about the series.** 
{: .text-color}

![Bizenghast]({{'/assets/img/Bizenghastvolone.jpg' | absolute_url}} "Bizenghast") 

![Bizenghast]({{'/assets/img/Bizenghastvoltwo.jpg' | absolute_url}} "Bizenghast") 

![Bizenghast]({{'/assets/img/deathdress.jpg' | absolute_url}} "Bizenghast") 

![Bizenghast]({{'/assets/img/Bizenghasthead.jpg' | absolute_url}} "Bizenghast") 